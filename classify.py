#!/home/daniel/work/tf/bin/python

from __future__ import absolute_import, division, print_function, unicode_literals
import matplotlib.pylab as plt
import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.keras import layers
import numpy as np
import PIL.Image as Image

num_threads=12
tf.config.threading.set_inter_op_parallelism_threads(num_threads)

IMAGE_SIZE=224
IMAGE_SHAPE = (IMAGE_SIZE, IMAGE_SIZE)
data_root = tf.keras.utils.get_file( 'vehicle_images','file:///vehicle_images.tgz', untar=True)
image_generator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1/255)
image_data = image_generator.flow_from_directory(str(data_root), target_size=IMAGE_SHAPE)
for image_batch, label_batch in image_data:
  print("Image batch shape: ", image_batch.shape)
  print("Label batch shape: ", label_batch.shape)
  break

#feature_extractor_url = "https://tfhub.dev/google/tf2-preview/inception_v3/feature_vector/4"
feature_extractor_url = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
#feature_extractor_url = "https://tfhub.dev/google/imagenet/inception_resnet_v2/feature_vector/4"
feature_extractor_layer = hub.KerasLayer(feature_extractor_url, input_shape=(IMAGE_SIZE,IMAGE_SIZE,3))
feature_batch = feature_extractor_layer(image_batch)
feature_extractor_layer.trainable = False
model = tf.keras.Sequential([ feature_extractor_layer, layers.Dense(image_data.num_classes, activation='softmax') ])
model.compile( optimizer=tf.keras.optimizers.Adam(), loss='categorical_crossentropy', metrics=['acc'])

class CollectBatchStats(tf.keras.callbacks.Callback):
  def __init__(self):
    self.batch_losses = []
    self.batch_acc = []

  def on_train_batch_end(self, batch, logs=None):
    self.batch_losses.append(logs['loss'])
    self.batch_acc.append(logs['acc'])
    self.model.reset_metrics()

steps_per_epoch = np.ceil(image_data.samples/image_data.batch_size)
batch_stats_callback = CollectBatchStats()
history = model.fit(image_data, epochs=50,
                    steps_per_epoch=steps_per_epoch,
                    callbacks = [batch_stats_callback])
import time
t = time.time()

export_path = "saved_models/{}".format(int(t))
model.save(export_path, save_format='tf')
